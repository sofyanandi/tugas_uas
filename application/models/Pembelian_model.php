<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian_model extends CI_Model 
{
		
	private $_table = "pembelian_header";
	//panggil nama table
	private $_table_header = "pembelian_header";
	private $_table_detail = "pembelian_detail";
	
	public function tampilDataPembelian()
	
	{
	
		return $this->db->get($this->_table_header)->result();
		
	}
	
	public function savePembelianHeader()
	{
		$data['no_transaksi']					= $this->input->post('no_transaksi');
		$data['kode_supplier']					= $this->input->post('kode_supplier');
		$data['tanggal']						= date('Y-m-d');
		$data['approved']						= 1;
		$data['flag']							= 1;
		
		$this->db->insert($this->_table_header, $data);
	}
	
	public function idTransaksiTerakhir()
	
	{
		$query = $this->db->query(
			"SELECT * FROM " . $this->_table_header . " WHERE flag = 1 ORDER BY id_pembelian_h	 DESC LIMIT 0,1"
		);
		$data_id = $query->result();
		
		foreach ($data_id as $data) {
			$last_id = $data->id_pembelian_h;	
		}
		return $last_id;
	}
	
	public function tampilDataPembelianDetail($id_pembelian_header)
	
	{
		$query = $this->db->query(
		"SELECT A. *, B.nama_barang FROM " . $this->_table_detail . " AS A INNER JOIN 
		barang AS B ON A.kode_barang = B.kode_barang WHERE A.flag = '1' AND A.id_pembelian_h = ". $id_pembelian_header
		);	
		return $query->result();
		
	}
	
	public function tampil_list_report($tgl_awal, $tgl_akhir)
	
	{
		$this->db->select("ph.id_pembelian_h, ph.no_transaksi, ph.tanggal, count(pd.kode_barang) 
		as total_barang, sum(pd.qty) as total_qty, sum(pd.jumlah) as total_pembelian");
		
		$this->db->from("pembelian_header ph");
		$this->db->join("pembelian_detail pd", "ph.id_pembelian_h = pd.id_pembelian_h");
		$this->db->where("ph.tanggal BETWEEN '$tgl_awal' AND '$tgl_akhir'");
		$this->db->group_by("ph.id_pembelian_h");
		$query	= $this->db->get();
		
		return $query->result();
		
	}
	
	public function createKodeUrut() 
		{	
			date_default_timezone_set("Asia/Jakarta");
			$this->db->select('MAX(no_transaksi) as no_transaksi');
			$query 		= $this->db->get($this->_table);
			$result		= $query->row_array(); // hasil berbentuk array
		
			$kode_transaksi_terakhir	= $result[('no_transaksi')];
			
			$label	= "TR";	
			$no_urut_lama	= (int) substr($kode_transaksi_terakhir, 8, 2);
			$no_urut_lama	++;
			
			$no_urut_baru		= sprintf("%02s", $no_urut_lama);
			$tahun = (int) substr(date('y'), 1, 1);
			$bulan = date('m');
			$jam = date('H');
			if (($jam % 2) == 0) { $m = 'A';} else {$m = 'B';}
			$kode_jabatan_baru	= $label . $tahun .$bulan . $jam .$m. $no_urut_baru;
			
			
			// var_dump($kode_barang_baru); die();
			// var_dump (sprintf("%03s",0)); die();
			
			return $kode_jabatan_baru;
		
		}
	
	
	public function savePembelianDetail($id_pembelian_header)
	
	{
		$qty	=$this->input->post('qty');
		$harga	=$this->input->post('harga_barang');
		
		$data['id_pembelian_h']			= $id_pembelian_header;
		$data['kode_barang']			= $this->input->post('kode_barang');	
		$data['qty']					= $qty;
		$data['harga']					= $harga;
		$data['jumlah']					= $qty * $harga;
		$data['flag']					= 1;
		
		$this->db->insert($this->_table_detail, $data);
		
	}
	
	public function delete($id_pembelian_h)
	
	{
		$this->db->where('id_pembelian_h',$id_pembelian_h);
		$this->db->delete($this->_table_header);
	}
	
	public function rules_header()
	{
	
		return[
			[
				'field'  	=> 'no_transaksi',
				'label'  	=> 'Nomor Transaksi',
				'rules'  	=> 'required',
				'errors'	=> [
									'required'	=> 'no transaksi tidak boleh kosong. '
							]
			],
			
			[
				'field'  	=> 'kode_supplier',
				'label'  	=> 'Nama Supplier',
				'rules'  	=> 'required',
				'errors'	=> ['required'	=> 'Nama supplier tidak boleh kosong. ']
			]
			
		];
	
	}
	
	public function rules_detail()
	{
	
		return[
			[
				'field'  	=> 'qty',
				'label'  	=> 'Qty',
				'rules'  	=> 'required|numeric',
				'errors'	=> ['required'	=> 'Qty tidak boleh kosong. ','numeric' => 'Qty Harus Angka. ']							
			],
			
			[
				'field'  	=> 'harga_barang',
				'label'  	=> 'Harga Barang',
				'rules'  	=> 'required|numeric',
				'errors'	=> ['required'	=> 'Harga Barang tidak boleh kosong. ','numeric' => 'Harga Harus Angka. ']
			],
			
		];
	
	}
	
}
