<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan_model extends CI_Model 
{
	private $_table = "karyawan";
	
	public function tampilDataKaryawan()
	{
		return $this->db->get($this->_table)->result();
	}
	public function tampilDataKaryawan2()
	{	
		$query =$this->db->query("SELECT * FROM karyawan WHERE flag = 1");
		return $query->result();
	}
	public function tampilDataKaryawan3()
	{
		$this->db->select('*');
		$this->db->order_by('nik','ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function detail($nik)
	{
		$this->db->select('*');
		$this->db->where ('nik', $nik);
		$this->db->where('flag',1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function save ()
	{
		$nik_karyawan	= $this->input->post('nik');
		
		$data['nik']					= $nik_karyawan;
		$data['nik']					= $this->input->post('nik');
		$data['nama_lengkap']			= $this->input->post('nama_karyawan');
		$data['tempat_lahir']			= $this->input->post('tempat_lahir');
		$data['tgl_lahir']				= $this->input->post('tgl_lahir');
		$data['jenis_kelamin']			= $this->input->post('jk');
		$data['alamat']					= $this->input->post('alamat');
		$data['telp']					= $this->input->post('telp');
		$data['kode_jabatan']			= $this->input->post('kode_jabatan');
		$data['flag']					= 1;
		$data['photo']					= $this->uploadPhoto($nik_karyawan);
		$this->db->insert($this->_table, $data);
	}
	
	public function update($nik)
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabung = $thn . "-" . $bln . "-" . $tgl;
		
		$data['nama_lengkap']			= $this->input->post('nama_karyawan');
		$data['tempat_lahir']			= $this->input->post('tempat_lahir');
		$data['tgl_lahir']				= $this->input->post('tgl_lahir');
		$data['jenis_kelamin']			= $this->input->post('jenis_kelamin');
		$data['alamat']					= $this->input->post('alamat');
		$data['telp']					= $this->input->post('telp');
		$data['kode_jabatan']			= $this->input->post('kode_jabatan');
		$data['flag']					= 1;
		
		if (!empty($_FILES["image"]["name"])) {
			$this->hapusPhoto($nik);
			$foto_karyawan			= $this->uploadPhoto($nik);
		}	else {
			$foto_karyawan			= $this->input->post('foto_old');
		}
			
		$data['photo']				= $foto_karyawan;
		
		$this->db->where('nik', $nik);
		$this->db->update($this->_table, $data);
	}
	
	public function delete($nik)
	
	{
		$this->hapusPhoto($nik);
		$this->db->where('nik',$nik);
		$this->db->delete($this->_table);
		
	}
	
	private function uploadPhoto($nik)
	{
		$date_upload				= date('Ymd');
		$config['upload_path']		= './resources/foto_karyawan/';
		$config['allowed_types']	= 'gif|jpg|png|jpeg';
		$config['file_name']		= $date_upload . '_' . $nik;
		$config['overwrite']		= true;
		$config['max_size']			= 1024;
		
		/*echo"<pre>";
		print_r($_FILES["image"]); die();
		echo"/pre";*/
		
		$this->load->library('upload', $config);
		
		if ($this->upload->do_upload('image')){
			$nama_file	= $this->upload->data("file_name");
		} else {
			$nama_file	= "default.jpeg";
		}
		
		return $nama_file;
	}
	
	private function hapusPhoto($nik)
	{
		// cari name file
		$data_karyawan 	= $this->detail($nik);
		foreach ($data_karyawan as $data){
		$nama_file		=	$data->photo;	
		}
		
		if ($nama_file != "default.jpeg") {
			$path = "./resources/foto_karyawan/ . $nama_file";
			// bentuk path nya : ./ resorces.foto_karyawan/20190328.jpeg
			return @unlink($path);
			}
	}
	
	public function nik() 
		{
			$query = $this->db->query("SELECT * FROM karyawan ORDER by nik DESC LIMIT 1");
			return $query->result();
		}
	
		public function rules()
	{
	
		return[
			[
				'field'  	=> 'nik',
				'label'  	=> 'NIK',
				'rules'  	=> 'required',
				'errors'	=> [
									'required'	=> 'NIK tidak boleh kosong. '
							]
			],
			
			[
				'field'  	=> 'nama_karyawan',
				'label'  	=> 'Nama Karyawan',
				'rules'  	=> 'required',
				'errors'	=> ['required'	=> 'Nama Karyawan tidak boleh kosong. ']
			],
			
			[
				'field'  	=> 'tempat_lahir',
				'label'  	=> 'Tempat Lahir',
				'rules'  	=> 'required',
				'errors'	=> ['required'	=> 'Tempat Lahir tidak boleh kosong. ']
			],
			
			[
				'field'  	=> 'telp',
				'label'  	=> 'Telepon',
				'rules'  	=> 'required',
				'errors'	=> ['required'	=> 'Nomer Telepon tidak boleh kosong. ']
			],
			
			[
				'field'  	=> 'alamat',
				'label'  	=> 'Alamat',
				'rules'  	=> 'required',
				'errors'	=> ['required'	=> 'Alamat tidak boleh kosong. ']
			]
			
			/*[
				'field'  	=> 'stok',
				'label'  	=> 'Stok Barang',
				'rules'  	=> 'required|numeric',
				'errors'	=> ['required'	=> 'Stok Barang tidak boleh kosong. ' , 'numeric' => 'Stok Barang Harus Angka. ' ]
			],*/
			
		];
	
	}
	
	public function tampilDataKaryawanPagination($perpage, $uri, $data_pencarian_karyawan)
	{
		$this->db->select('*');
		if (!empty($data_pencarian_karyawan))
			{
				$this->db->like('nama_lengkap', $data_pencarian_karyawan);
			} 
			
				$this->db->order_by('nik','asc');
			
			$get_data	= $this->db->get($this->_table, $perpage, $uri);
			if ($get_data->num_rows() > 0) 
			{ 
				return $get_data->result();
			} 
			else 
			{
				return null;
			}
	}	
	public function tombolpagination($data_pencarian_karyawan)
	
	{
		// cari jumlah data berdasarkan data pencarian
		$this->db->like('nama_lengkap', $data_pencarian_karyawan);
		$this->db->from($this->_table);
		$hasil = $this->db->count_all_results();
		
		// penjualan limit
		$pagination['base_url'] 	= base_url().'karyawan/listKaryawan/load/';
		$pagination['total_rows']	= $hasil;
		$pagination['per_page']		= "3";
		$pagination['uri_segment']	= 4;
		$pagination['num_links']	= 2;
		
		// custom paging cunfiguration
		
		$pagination['full_tag_open']	= '<div class="pagination">"';
		$pagination['full_tag_close']	= '</div>';
		
		$pagination['first_link']		= 'First Page';
		$pagination['first_tag_open']	= '<span class="firstlink">';
		$pagination['first_tag_close']	= '</span>';
		
		$pagination['last_link']		= 'Last Page';
		$pagination['last_tag_open']	= '<span class="lastlink">';
		$pagination['last_tag_close']	= '</span>';
		
		$pagination['next_link']		= 'Next Page';
		$pagination['next_tag_open']	= '<span class="nextlink">';
		$pagination['next_tag_close']	= '</span>';
		
		$pagination['prev_link']		= 'Prive Page';
		$pagination['prev_tag_open']	= '<span class="prevelink">';
		$pagination['prev_tag_close']	= '</span>';
		
		$pagination['cur_tag_open']		= '<span class="prevlink">';
		$pagination['cur_tag_close']	= '</span>';
		
		$pagination['num_tag_open']		= '<span class="numlink"';
		$pagination['num_tag_close']	= '</span>';
		
		$this->pagination->initialize($pagination);
			
		$hasil_pagination	= $this->tampilDataKaryawanPagination($pagination['per_page'],
		$this->uri->segment(4), $data_pencarian_karyawan);
		
		return $hasil_pagination;
		
	}
}