<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan_model extends CI_Model 
{
		
	
	//panggil nama table
	private $_table_header = "penjualan_header";
	private $_table_detail = "penjualan_detail";
	
	public function tampilDataPenjualan()
	
	{
	
		return $this->db->get($this->_table_header)->result();
		
	}
	
	public function savePenjualanHeader()
	{
		$data['no_transaksi']					= $this->input->post('no_transaksi');
		$data['tanggal']						= date('Y-m-d');
		$data['pembeli']						= $this->input->post('pembeli');
		$data['flag']							= 1;
		
		$this->db->insert($this->_table_header, $data);
	}
	
	public function idTransaksiTerakhir()
	
	{
		$query = $this->db->query(
			"SELECT * FROM " . $this->_table_header . " WHERE flag = 1 ORDER BY id_jual_h	 DESC LIMIT 0,1"
		);
		$data_id = $query->result();
		
		foreach ($data_id as $data) {
			$last_id = $data->id_jual_h;	
		}
		return $last_id;
	}
	
	public function tampilDataPenjualanDetail($id_penjualan_h)
	
	{
		$query = $this->db->query(
		"SELECT A. *, B.nama_barang FROM " . $this->_table_detail . " AS A INNER JOIN 
		barang AS B ON A.kode_barang = B.kode_barang WHERE A.flag = '1' AND A.id_jual_h = ". $id_penjualan_h
		);	
		return $query->result();
		
	}
	
	public function tampil_list_report_penjualan($tgl_awal, $tgl_akhir)
	
	{
		$this->db->select("ph.id_jual_h, ph.no_transaksi, ph.tanggal, count(pd.kode_barang) 
		as total_barang, sum(pd.qty) as total_qty, sum(pd.jumlah) as total_penjualan");
		
		$this->db->from("penjualan_header ph");
		$this->db->join("penjualan_detail pd", "ph.id_jual_h = pd.id_jual_h");
		$this->db->where("ph.tanggal BETWEEN '$tgl_awal' AND '$tgl_akhir'");
		$this->db->group_by("ph.id_jual_h");
		$query	= $this->db->get();
		
		return $query->result();
		
	}
	
	public function createKodeUrut() 
		{	
			date_default_timezone_set("Asia/Jakarta");
			$this->db->select('MAX(no_transaksi) as no_transaksi');
			$query 		= $this->db->get($this->_table_header);
			$result		= $query->row_array(); // hasil berbentuk array
		
			$kode_transaksi_terakhir	= $result[('no_transaksi')];
			
			$label	= "TR";	
			$no_urut_lama	= (int) substr($kode_transaksi_terakhir, 8, 2);
			$no_urut_lama	++;
			
			$no_urut_baru		= sprintf("%02s", $no_urut_lama);
			$tahun = (int) substr(date('y'), 1, 1);
			$bulan = date('m');
			$jam = date('H');
			if (($jam % 2) == 0) { $m = 'A';} else {$m = 'B';}
			$kode_jabatan_baru	= $label . $tahun .$bulan . $jam .$m. $no_urut_baru;
			
			
			// var_dump($kode_barang_baru); die();
			// var_dump (sprintf("%03s",0)); die();
			
			return $kode_jabatan_baru;
		
		}
	
	
	public function savePenjualanDetail($id)
	
	{
		$qty	=$this->input->post('qty');
		$harga	=$this->input->post('harga_barang');
		$kode_barang	=$this->input->post('kode_barang');
		$harga_barang	=$this->barang_model->cariHargaBarang($kode_barang);

		
		$data['id_jual_h']				= $id;
		$data['kode_barang']			= $kode_barang;	
		$data['qty']					= $qty;
		$data['harga']					= $harga_barang;
		$data['jumlah']					= $qty * $harga_barang;
		$data['flag']					= 1;
		
		$this->db->insert($this->_table_detail, $data);
		
	}
	
	public function delete($id_penjualan_h)
	
	{
		$this->db->where('id_jual_h',$id_penjualan_h);
		$this->db->delete($this->_table_header);
	}
	
	public function rules_header()
	{
	
		return[
			[
				'field'  	=> 'no_transaksi',
				'label'  	=> 'Nomor Transaksi',
				'rules'  	=> 'required',
				'errors'	=> [
									'required'	=> 'no transaksi tidak boleh kosong. '
							]
			],
			
			[
				'field'  	=> 'pembeli',
				'label'  	=> 'Nama Pembeli',
				'rules'  	=> 'required',
				'errors'	=> ['required'	=> 'Nama pembeli tidak boleh kosong. ']
			]
			
		];
	
	}
	
	public function rules_detail()
	{
	
		return[

			[
				'field'  	=> 'kode_barang',
				'label'  	=> 'Kode Barang',
				'rules'  	=> 'required|max_length[5]',
				'errors'	=> ['required'	=> 'Kode Barang tidak boleh kosong. ', 'max_length' => 'Kode Barang harus angka']
			],



			[
				'field'  	=> 'qty',
				'label'  	=> 'Qty',
				'rules'  	=> 'required|numeric',
				'errors'	=> ['required'	=> 'Qty tidak boleh kosong. ','numeric' => 'Qty Harus Angka. ']							
			],
			
						
		];
	
	}
	
}
