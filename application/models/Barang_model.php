<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_model extends CI_Model 
{
	private $_table = "barang";
	
	public function tampilDataBarang()
	{
		return $this->db->get($this->_table)->result();
	}
	public function tampilDataBarang2()
	{
	$query =$this->db->query("SELECT * FROM barang as br inner join jenis_barang as jb on br.kode_jenis=jb.kode_jenis");
		return $query->result();
	}
	public function tampilDataBarang3()
	{
		$this->db->select('*');
		$this->db->order_by('kode_barang','ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	
	public function detail($kode_barang)
	{
		$this->db->select('*');
		$this->db->where ('kode_barang', $kode_barang);
		$this->db->where('flag',1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function save ()
	{
		$data['kode_barang']			= $this->input->post('kode_barang');
		$data['nama_barang']			= $this->input->post('nama_barang');
		$data['harga_barang']			= $this->input->post('harga_barang');
		$data['kode_jenis']				= $this->input->post('kode_jenis');
		$data['flag']					= 1;
		$this->db->insert($this->_table, $data);
	}
	
	public function update($kode_barang)
	{
		$data['nama_barang']			= $this->input->post('nama_barang');
		$data['harga_barang']			= $this->input->post('harga_barang');
		$data['kode_jenis']				= $this->input->post('kode_jenis');
		$data['flag']					= 1;
		
		$this->db->where('kode_barang', $kode_barang);
		$this->db->update($this->_table, $data);
	}
	public function delete($kode_barang)
	
	{
		$this->db->where('kode_barang',$kode_barang);
		$this->db->delete($this->_table);
	}
	
	public function updateStok($kode_barang, $qty)
	{
		//panggil data detail stok
		$cari_stok =$this->detail($kode_barang);
		foreach ($cari_stok as $data) 
		{$stok =$data->stok;}
		
		$jumlah_stok				= $stok + $qty;
		$data_barang['stok']		= $jumlah_stok;
		
		$this->db->where('kode_barang', $kode_barang);
		$this->db->update('barang', $data_barang);
	}

	public function updateStokJual($kode_barang, $qty)
	{
		//panggil data detail stok
		$cari_stok =$this->detail($kode_barang);
		foreach ($cari_stok as $data) 
		{$stok =$data->stok;}
		
		$jumlah_stok				= $stok - $qty;
		$data_barang['stok']		= $jumlah_stok;
		
		$this->db->where('kode_barang', $kode_barang);
		$this->db->update('barang', $data_barang);
	}
	
	public function tampilDataBarangPagination($perpage, $uri, $data_pencarian)
	{
		$this->db->select(
		
		'barang.kode_barang,barang.nama_barang,barang.harga_barang,barang.stok,barang.kode_jenis,
		jenis_barang.kode_jenis,jenis_barang.nama_jenis');
		$this->db->join('jenis_barang','jenis_barang.kode_jenis=barang.kode_jenis');
		if (!empty($data_pencarian))
			{
				$this->db->like('nama_barang', $data_pencarian);
			} 
			
				$this->db->order_by('kode_barang','asc');
			
			$get_data	= $this->db->get($this->_table, $perpage, $uri);
			if ($get_data->num_rows() > 0) 
			{ 
				return $get_data->result();
			} 
			else 
			{
				return null;
			}
	}
	
	public function createKodeUrut() 
		{
			$this->db->select('MAX(kode_barang) as kode_barang');
			$query 		= $this->db->get($this->_table);
			$result		= $query->row_array(); // hasil berbentuk array
		
			$kode_barang_terakhir	= $result[('kode_barang')];
			
			$label	= "BR";
			$no_urut_lama	= (int) substr($kode_barang_terakhir, 2, 3);
			$no_urut_lama	++;
			
			$no_urut_baru		= sprintf("%03s", $no_urut_lama);
			$kode_barang_baru	= $label . $no_urut_baru;
			
			// var_dump($kode_barang_baru); die();
			// var_dump (sprintf("%03s",0)); die();
			
			return $kode_barang_baru;
		
		}

	public function cariHargaBarang($kode_barang)
	 
	{
		$query = $this->db->query("SELECT * FROM " . $this->_table . " WHERE flag = 1 AND kode_barang = '$kode_barang'");
		$hasil = $query->result();
		
		foreach ($hasil as $data){ $harganya =$data->harga_barang;}
		return $harganya;
	}
	
	public function tombolpagination($data_pencarian)
	
	{
		// cari jumlah data berdasarkan data pencarian
		$this->db->like('nama_barang', $data_pencarian);
		$this->db->from($this->_table);
		$hasil = $this->db->count_all_results();
		
		// penjualan limit
		$pagination['base_url'] 	= base_url().'barang/listBarang/load/';
		$pagination['total_rows']	= $hasil;
		$pagination['per_page']		= "3";
		$pagination['uri_segment']	= 4;
		$pagination['num_links']	= 2;
		
		// custom paging cunfiguration
		
		$pagination['full_tag_open']	= '<div class="pagination">"';
		$pagination['full_tag_close']	= '</div>';
		
		$pagination['first_link']		= 'First Page';
		$pagination['first_tag_open']	= '<span class="firstlink">';
		$pagination['first_tag_close']	= '</span>';
		
		$pagination['last_link']		= 'Last Page';
		$pagination['last_tag_open']	= '<span class="lastlink">';
		$pagination['last_tag_close']	= '</span>';
		
		$pagination['next_link']		= 'Next Page';
		$pagination['next_tag_open']	= '<span class="nextlink">';
		$pagination['next_tag_close']	= '</span>';
		
		$pagination['prev_link']		= 'Prive Page';
		$pagination['prev_tag_open']	= '<span class="prevelink">';
		$pagination['prev_tag_close']	= '</span>';
		
		$pagination['cur_tag_open']		= '<span class="prevlink">';
		$pagination['cur_tag_close']	= '</span>';
		
		$pagination['num_tag_open']		= '<span class="numlink"';
		$pagination['num_tag_close']	= '</span>';
		
		$this->pagination->initialize($pagination);
			
		$hasil_pagination	= $this->tampilDataBarangPagination($pagination['per_page'],
		$this->uri->segment(4), $data_pencarian);
		
		return $hasil_pagination;
		
	}
	
	public function rules()
	{
	
		return[
			[
				'field'  	=> 'kode_barang',
				'label'  	=> 'Kode Barang',
				'rules'  	=> 'required|max_length[5]',
				'errors'	=> [
									'required'	=> 'Kode Barang tidak boleh kosong. ',
									'max_length'	=> 'Kode Barang tidak boleh lebih Dari 5 Karakter. '
							]
			],
			
			[
				'field'  	=> 'nama_barang',
				'label'  	=> 'Nama Barang',
				'rules'  	=> 'required',
				'errors'	=> ['required'	=> 'Nama Barang tidak boleh kosong. ']
			],
			
			[
				'field'  	=> 'harga_barang',
				'label'  	=> 'Harga Barang',
				'rules'  	=> 'required|numeric',
				'errors'	=> ['required'	=> 'Harga Barang tidak boleh kosong. ','numeric' => 'Harga Harus Angka. ']
			],
			
			[
				'field'  	=> 'kode_jenis',
				'label'  	=> 'Kode Jenis',
				'rules'  	=> 'required',
				'errors'	=> ['required'	=> 'Kode Jenis tidak boleh kosong. ']
			]
			
			/*[
				'field'  	=> 'stok',
				'label'  	=> 'Stok Barang',
				'rules'  	=> 'required|numeric',
				'errors'	=> ['required'	=> 'Stok Barang tidak boleh kosong. ' , 'numeric' => 'Stok Barang Harus Angka. ' ]
			],*/
			
		];
	
	}
	
}