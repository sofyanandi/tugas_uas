<div class="menu-malasngoding">
		<ul>
			<li><a href="<?=base_url();?>home">Home</a></li>
			<li class="dropdown"><a href="#">Laporan</a>
            	<ul class="isi-dropdown">
            		<li><a href="<?=base_url();?>pembelian/input_report">Pembelian</a></li>
            		<li><a href="<?=base_url();?>penjualan/input_report_penjualan">Penjualan</a></li>
                </ul>
            </li>
			<li class="dropdown"><a href="#">Data</a>
				<ul class="isi-dropdown">
					<li><a href="<?=base_url();?>karyawan/listkaryawan">Karyawan</a></li>
					<li><a href="<?=base_url();?>jabatan/listjabatan">Jabatan</a></li>
					<li><a href="<?=base_url();?>barang/listbarang">Barang</a></li>
					<li><a href="<?=base_url();?>jenis_barang/listjenisbarang">Jenis Barang</a></li>
                    <li><a href="<?=base_url();?>supplier/listsupplier">Supplier</a></li>
				</ul>
			</li>
            <li class="dropdown"><a href="#">Transaksi</a>
            <ul class="isi-dropdown">
					<li><a href="<?=base_url();?>pembelian/listpembelian">Pembelian</a></li>
					<li><a href="<?=base_url();?>penjualan/listpenjualan">Penjualan</a></li>
				</ul>
              </li>
			<li><a onClick="return confirm('Anda Yakin Ingin Keluar')" href="<?=base_url();?>auth/logout">Logout</a></li>
		</ul>

	</div>