<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>jQuery UI Datepicker - Default functionality</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
   $( function(){
	  $( "#tgl_awal" ).datepicker({dateFormat: "yy-mm-dd"});
	  $( "#tgl_akhir" ).datepicker({dateFormat: "yy-mm-dd"});
	  });
	
  </script>
<script>
  $(document).ready(function(){

    $('#proses').on('click', function(event) {
    event.preventDefault();
    var tgl_awal = $('#tgl_awal').val();
    var tgl_akhir = $('#tgl_akhir').val();

    if (tgl_awal == '' || tgl_akhir == '') {
      alert('Tanggal Tidak Boleh Kosong');
    }else if (new Date(tgl_awal) > new Date(tgl_akhir)) {
      alert('Format Waktu Salah Input');
    }else{
      $('#forms').submit();
    }
    });

  });
</script>
  </head>
<body>
 <form action="<?=base_url();?>pembelian/list_report" method="POST" id="forms">
 <table align="center" border="0" bgcolor="white" width="500px" height="180px">
	 <tr>
 		<td>Tanggal Awal</td> 
 		<td>:</td>
 		<td><input type="text" name="tgl_awal" id="tgl_awal"> </td>
	</tr>
    
	<tr>
    <td>Tanggal Terakhir</td>
    <td>:</td>
    <td><input type="text" name="tgl_akhir" id="tgl_akhir"></td>
    </tr>
    	
    <tr>
    <td><?= validation_errors();?></td>
    <td>&nbsp;</td>
    <td>
    <input type="submit" name="proses" id="proses" value="Proses">
      <input type="reset" name="reset" id="reset" value="Reset">
     </td>
  </tr>
</table>
</form>

</body>
</html>
