<?php
	foreach($detail_barang as $data) 
	{
    	$kode_barang	= $data->kode_barang;
		$nama_barang	= $data->nama_barang;
		$harga			= $data->harga_barang;
		$kode_jenis		= $data->kode_jenis;
    }
?>

<body bgcolor="#999999">
<center><font color="#FFFFFF" size="+3" >Edit Barang</font></center><br>
<div id="body" style="text-align: center;">
<div style="color:white"><?= validation_errors();?></div>
<form action="<?=base_url()?>barang/editbarang/<?=$kode_barang?>" method="POST";>
<table width="40%" border="0" cellpadding="5" bgcolor="#FFFFFF" align="center">
  <tr>
    <td>Kode Barang</td>
    <td>:</td>
    <td><input <input value ="<?=$kode_barang;?>" type="text" name="kode_barang" id="kode_barang" maxlength="10" value="<?= set_value('kode_barang');?>"></td>
  </tr>
  <tr>
    <td>Nama Barang</td>
    <td>:</td>
    <td><input <input value ="<?=$nama_barang;?>" type="text" name="nama_barang" id="nama_barang" maxlength="50" value="<?= set_value('nama_barang');?>"></td>
  </tr>
  <tr>
    <td>Harga Barang</td>
    <td>:</td>
    <td><input <input value ="<?=$harga;?>" type="text" name="harga_barang" id="harga_barang" maxlength="50" value="<?= set_value('harga_barang');?>">
    </td>
  </tr>
  <tr>
    <td>Kode Jenis</td>
    <td>:</td>
    <td><select name="kode_jenis" id="kode_jenis">
    	<?php foreach($data_jenis_barang as $data) { 
				$select_jenis = ($data->kode_jenis == $kode_jenis) ? 'selected' : '';
		?>
    	<option value="<?=$data->kode_jenis;?>" <?=$select_jenis;?>><?=$data->nama_jenis;?></option>
        <?php }?>
    	</select>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="submit" id="submit" value="Simpan">
      <input type="reset" name="reset" id="reset" value="Reset"><br/><br/>
      <a href="<?=base_url();?>barang/listbarang""><input type="button" name="button" id="button" value="Kembali Ke Menu Sebelumnya"></a></td>
  </tr>
</table>
</form>
</body>