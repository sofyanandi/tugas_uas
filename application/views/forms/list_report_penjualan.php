<center><font size="+2" color="#FFFFFF"> Laporan Penjualan </font> </center>
<center><font size="+2" color="#FFFFFF"> Dari Tanggal <?=$tgl_awal;?> sd  <?=$tgl_akhir;?> </font> </center><br />

 <?php
        if ($this->session->flashdata('info') == true) {
            echo $this->session->flashdata('info');
            }
?>
    
    <table  align="center" width="80%" border="1" cellspacing="0" cellpadding="5" bordercolor="#FFFFFF" bgcolor="#FFFFFF">
        <tr>
            <td colspan="8" ><font><a href="<?= base_url();?>penjualan/cetakPdf/<?=$tgl_awal;?>/<?=$tgl_akhir;?>">Cetak Pdf</a></font></td>
        </tr>
        <tr>
            <th>No</th>
            <th>ID Penjualan</th>
            <th>No Transaksi</th>  
            <th>Tanggal</th>
            <th>Total Barang</th>
            <th>Total qty</th>
            <th>Jumlah Nominal Penjualan</th>
        
            <th>Aksi</th>
        </tr>
        <?php
        $no = 0;
        $total  = 0;
        foreach ($data_penjualan as $data) { $no++;
      
        ?>
        <tr>
            
            <td><?=$no;?></td>
            <td><?=$data->id_jual_h;?></td>
            <td><?=$data->no_transaksi;?></td>
            <td><?=$data->tanggal;?></td>
            <td><?=$data->total_barang;?></td>
            <td><?=$data->total_qty;?></td>
            <td align="right">Rp. <?= number_format($data->total_penjualan)?></td>
            <td>
            <a onClick="return confirm('Anda Yakin Ingin Hapus Data')"href="<?= base_url();?>penjualan/delete/<?= $data->id_jual_h;?>">
       Delete</a>
            </td>
        </tr> 
            <?php
                $total += $data->total_penjualan;
    }
                
            ?>
            
                <tr align="center">
            <th align="right" colspan="6">Total Keseluruhan</th>
            
           <th align="right">Rp. <?= number_format($total);?></th>
        </tr>
    </table>
    