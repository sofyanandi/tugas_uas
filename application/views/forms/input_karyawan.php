<body bgcolor="#999999">
<div id="body" style="text-align: center;">
<div style="color:white"><?= validation_errors();?></div>
<form action="<?=base_url()?>karyawan/input_karyawan" method="POST" enctype="multipart/form-data">
<table width="1000px"  height="500px" border="0" cellpadding="5" bgcolor="#FFFFFF" align="center">
  <tr>
    <td>NIK</td>
    <td>:</td>
    <?php
		foreach($data_nik as $data) {
		$n1 = substr($data->nik,-3) +1;
		$hasil = date('y').date('m').sprintf('%03d' , $n1);	
		
	?>
    <td><input type="text" name="nik" id="nik" maxlength="10" value="<?= $hasil ;?>" readonly></td>
    <?php
		}
	?>
  </tr>
  <tr>
    <td>Nama Karyawan</td>
    <td>:</td>
    <td><input type="text" name="nama_karyawan" id="nama_karyawan" maxlength="50" value="<?= set_value('nama_karyawan');?>"></td>
  </tr>
  <tr>
    <td>Tempat Lahir</td>
    <td>:</td>
    <td><input type="text" name="tempat_lahir" id="tempat_lahir" maxlength="50" value="<?= set_value('tempat_lahir');?>">
    </td>
  </tr>
  <tr>
    <td>Jenis Kelamin</td>
    <td>:</td>
    <td><select name="jk" id="jenis_kelamin">
    	<option value="P">Perempuan</option>
        <option value="L">Laki-laki</option>
    </select>
    </td>
  </tr>
  
  <tr>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
   $( function(){
	  $( "#tgl_lahir" ).datepicker({dateFormat: "yy-mm-dd"});
	  });
  </script>
 		<td>Tanggal Lahir</td> 
 		<td>:</td>
 		<td><input type="text" name="tgl_lahir" id="tgl_lahir"> </td>
	</tr>
  <tr>
    <td>Telepon</td>
    <td>:</td>
    <td><input type="text" name="telp" id="telp" maxlength="50" value="<?= set_value('telepon');?>"></td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><textarea name="alamat" id="alamat" cols="45" rows="5" value="<?= set_value('alamat');?>"></textarea></td>
  </tr>
  <tr>
    <td>Jabatan</td>
    <td>:</td>
    <td><select name="kode_jabatan" id="jabatan">
    	<?php foreach($data_jabatan as $data) { ?>
    	<option value="<?=$data->kode_jabatan;?>">
		<?=$data->nama_jabatan;?></option>
        <?php }?>
    	</select>
    </td>
  </tr>
  <tr>
  <td>Upload Foto</td>
  <td>:</td>
  <td>
  <input type="file" name="image" id="image">
  </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="submit" id="submit" value="Simpan">
      <input type="reset" name="reset" id="reset" value="Reset"><br/><br/>
      <a href="<?=base_url();?>karyawan/listkaryawan""><input type="button" name="button" id="button" value="Kembali Ke Menu Sebelumnya"></a></td>
  </tr>
</table>
</form>
</body>