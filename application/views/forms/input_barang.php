<body bgcolor="#999999">
<center><font color="#FFFFFF" size="+2">Input Barang</font></center><br/>
<div id="body" style="text-align: center;">
<div style="color:white"><?= validation_errors();?></div>
<form action="<?=base_url()?>barang/input_barang" method="POST";>
<table width="40%" border="0" cellpadding="5" bgcolor="#FFFFFF" align="center">
  <tr>
    <td>Kode Barang</td>
    <td>:</td>
    <td><input type="text" name="kode_barang" id="kode_barang" maxlength="10" value="<?= $data_barang ;?>" readonly> </td>
  </tr>
  <tr>
    <td>Nama Barang</td>
    <td>:</td>
    <td><input type="text" name="nama_barang" id="nama_barang" maxlength="50" value="<?= set_value('nama_barang');?>"></td>
  </tr>
  <tr>
    <td>Harga Barang</td>
    <td>:</td>
    <td><input type="text" name="harga_barang" id="harga_barang" maxlength="50" value="<?= set_value('harga_barang');?>">
    </td>
  </tr>
  <tr>
    <td>Kode Jenis</td>
    <td>:</td>
    <td><select name="kode_jenis" id="kode_jenis">
    	<?php foreach($data_jenis_barang as $data) { ?>
    	<option value="<?=$data->kode_jenis;?>">
		<?=$data->nama_jenis;?></option>
        <?php }?>
    	</select>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="submit" id="submit" value="Simpan">
      <input type="reset" name="reset" id="reset" value="Reset"><br/><br/>
      <a href="<?=base_url();?>barang/listbarang""><input type="button" name="button" id="button" value="Kembali Ke Menu Sebelumnya"></a></td>
  </tr>
</table>
</form>
</body>