<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends CI_Controller {
	
	public function __construct()
	{
		parent:: __construct();
		$this->load->model("penjualan_model");
		$this->load->model("barang_model");
		$this->load->library("pdf");
	}
	
	public function index()
	{
		$this->listPenjualan();
	}
	public function listPenjualan()
	{
		$data['data_penjualan'] = $this->penjualan_model->tampilDataPenjualan();
		$data['content']		= 'forms/list_penjualan';
		$this->load->view('home', $data);
	}
	
	public function input_report_penjualan()
	{
		$data['content']		= 'forms/input_report_penjualan';
		$this->load->view('home', $data);
		
	}
	
	public function list_report_penjualan()
	
	{
		if (!empty($_REQUEST)) {
			//ambil proses tanggal
		$tgl_awal			= $this->input->post('tgl_awal');
		$tgl_akhir			= $this->input->post('tgl_akhir');
		$data['data_penjualan'] = $this->penjualan_model->tampil_list_report_penjualan($tgl_awal, $tgl_akhir);
	
		$data['tgl_awal'] =  $tgl_awal;
		$data['tgl_akhir'] =  $tgl_akhir;	
		
		
		$data['content']		= 'forms/list_report_penjualan';
		$this->load->view('home', $data);
		} else {
			riderect("penjualan/input_report_penjualan");	
		}
	}

	public function input_penjualan()
	{	
		
		$data['kode_transaksi'] = $this->penjualan_model->createKodeUrut();
		$data['content']		= 'forms/input_penjualan';
		
		/*if (!empty($_REQUEST)) {
				$m_pembelian_h = $this->pembelian_model;
				$m_pembelian_h->savePembelianHeader();
				$id_terakhir = array();
				
				$id_terakhir = $m_pembelian_h->idTransaksiTerakhir();
				
				redirect("pembelian/input_pembelian_detail/" . $id_terakhir, "refresh");	
			}
		*/
		$validation = $this->form_validation;
		$validation->set_rules($this->penjualan_model->rules_header());
		
		if ($validation->run()) {
			$m_penjualan_h = $this->penjualan_model;
			$id_terakhir = array();
			$id_terakhir = $m_penjualan_h->idTransaksiTerakhir();
			
			$this->penjualan_model->savePenjualanHeader();
			$this->session->set_flashdata('info', '<div style="color: white">Simpan Data Berhasil !</div>');
				
			redirect("penjualan/input_penjualan_detail/" . $id_terakhir, "refresh");	
			}
		
		$this->load->view('home', $data);
	}
	
	public function input_penjualan_detail($id_penjualan_header)
	
	{	
		// panggil data barang untuk form input		
		$data['data_barang'] 	= $this->barang_model->tampilDataBarang();
		$data['id_header']		= $id_penjualan_header;
		$data['data_penjualan_detail'] = $this->penjualan_model->tampildataPenjualanDetail($id_penjualan_header);
		$data['content']		= 'forms/input_penjualan_detail';
		
		// proses simpan ke pembalian detail jika request from
			/*if (!empty($_REQUEST)) {
			// save detail
			$this->pembelian_model->savePembelianDetail($id_pembelian_header);
			
			//proses update stok
			
			$kode_barang	=$this->input->post('kode_barang');
			$qty		=$this->input->post('qty');
			$this->barang_model->updateStok($kode_barang, $qty);
			
			redirect("pembelian/input_pembelian_detail/" . $id_pembelian_header, "refresh");
			}*/
		$validation = $this->form_validation;
		$validation->set_rules($this->penjualan_model->rules_detail());
		
		if ($validation->run()) {
			$this->penjualan_model->savePenjualanDetail($id_penjualan_header);
			$this->session->set_flashdata('info', '<div style="color: white">Simpan Data Berhasil !</div>');
			
			$kode_barang	=$this->input->post('kode_barang');
			$qty		=$this->input->post('qty');
			$this->barang_model->updateStokJual($kode_barang, $qty);
			
			redirect("penjualan/input_penjualan_detail/" . $id_penjualan_header, "refresh");	
			}
		
		$this->load->view('home', $data);
	}
	
	public function delete($id_penjualan_h)
	{
		$m_penjualan = $this->penjualan_model;
		$m_penjualan->delete($id_penjualan_h);
		$this->session->set_flashdata('info', '<div style="color: white">Hapus Data Berhasil !</div>');	
		redirect("penjualan/index", "refresh");	
	}
	
	public function tesPdf()
	{
		$pdf = new FPDF('P', 'mm', 'A4');
		$pdf->AddPage();
		$pdf->setFont('Arial', 'B', 16);
		$pdf->Cell(50, 7, 'TOKO JAYA ABADI', 1, 0, 'C');
		$pdf->Output();
	}
	
	public function cetakPdf($tgl_awal, $tgl_akhir)
	{
		$pdf = new FPDF('P', 'mm', 'A4');
		$pdf->AddPage();
		$pdf->setFont('Arial', 'B', 16);
		$pdf->Cell(190, 7, 'TOKO JAYA ABADI', 0, 1, 'C');
		$pdf->setFont('Arial', 'B', 16);
		$pdf->Cell(190, 7, 'Dari Tanggal', 0, 1, 'C');
		$pdf->SetFont('Arial', 'B', 12);
		
		
		
		$pdf->Cell(190, 7, 'LAPORAN PENJUALAN BARANG', 0, 1, 'C');
		
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(20, 6, 'No', 1, 0, 'C');
		$pdf->Cell(35, 6, 'No Transaksi', 1, 0, 'C');
		$pdf->Cell(30, 6, 'Tanggal', 1, 0, 'C');
		$pdf->Cell(30, 6, 'Total Barang', 1, 0, 'C');
		$pdf->Cell(30, 6, 'Total Qty', 1, 0, 'C');
		$pdf->Cell(40, 6, 'Jumlah Nominal', 1, 1, 'C');
		$pdf->SetFont('Arial', '', 10);
		
		$no = 0;
		$total =0;
		$report = $this->penjualan_model->tampil_list_report_penjualan($tgl_awal,$tgl_akhir);
		foreach ($report as $data) {
			$no ++;
		$pdf->Cell(20, 6,$no, 1, 0, 'C');
		
		$pdf->Cell(35, 6,$data->no_transaksi, 1, 0, 'C');	
		$pdf->Cell(30, 6,$data->tanggal, 1, 0, 'C');	
		$pdf->Cell(30, 6,$data->total_barang, 1, 0, 'C');	
		$pdf->Cell(30, 6,$data->total_qty, 1, 0, 'C');
		$pdf->Cell(40, 6,'Rp. ' . number_format($data->total_penjualan), 1, 1, 'R');		
		
		$total += $data->total_penjualan;
				
			}
		// tampil akhir
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(145, 6, 'Total Keseluruhan', 1, 0, 'R');
		$pdf->Cell(40, 6, 'Rp. ' . number_format($total), 1, 1, 'R');
		$pdf->Output();
	}
}