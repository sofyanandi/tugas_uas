-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 10, 2019 at 11:29 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_jaya_abadi`
--
CREATE DATABASE IF NOT EXISTS `toko_jaya_abadi` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `toko_jaya_abadi`;

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `kode_barang` varchar(5) NOT NULL,
  `nama_barang` varchar(150) NOT NULL,
  `harga_barang` float NOT NULL,
  `kode_jenis` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL,
  `stok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`kode_barang`, `nama_barang`, `harga_barang`, `kode_jenis`, `flag`, `stok`) VALUES
('BR001', 'Laptop Asus Core I5', 4500000, 'JN002', 1, 26),
('BR002', 'Keyboard', 50000, 'JN002', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `kode_jabatan` varchar(5) NOT NULL,
  `nama_jabatan` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`kode_jabatan`, `nama_jabatan`, `keterangan`, `flag`) VALUES
('JB001', 'Kasir', 'Operational', 1),
('JB002', 'Admin', 'Operational', 1),
('JB003', 'Ofice Boy', 'Organisasi', 1),
('JB004', 'Pegawai', 'Staf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_barang`
--

CREATE TABLE `jenis_barang` (
  `kode_jenis` varchar(5) NOT NULL,
  `nama_jenis` varchar(150) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_barang`
--

INSERT INTO `jenis_barang` (`kode_jenis`, `nama_jenis`, `flag`) VALUES
('JN001', 'Alat Tulis Kantor', 1),
('JN002', 'Perangkat Keras', 1);

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `nik` varchar(10) NOT NULL,
  `nama_lengkap` varchar(150) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `kode_jabatan` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL,
  `photo` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`nik`, `nama_lengkap`, `tempat_lahir`, `tgl_lahir`, `jenis_kelamin`, `alamat`, `telp`, `kode_jabatan`, `flag`, `photo`) VALUES
('1902001', 'Adib Bisyri Musyaffa', 'Jakarta', '1998-12-11', 'L', 'Jl.Kalibaru Timur VC', '089621650023', 'JB001', 1, ''),
('1902002', 'Achmad Chaidir', 'Bekasi', '1998-04-11', 'L', 'Jl.Bekasi Utara', '08965878854', 'JB002', 1, ''),
('1902003', 'Sofyan Adi Dermawan', 'Jakarta', '1998-07-31', 'L', 'JL WARAKAS 4 gang 1 No 62 RT 002/ RW 003\r\nNo 62', '082246484578', 'JB003', 1, ''),
('1902004', 'Tofiko', 'Indonesia', '1998-04-14', 'L', 'JL BEKASI RAYA No 67', '089654123612', 'JB001', 1, 'default.jpeg'),
('1902005', 'Payung', 'Blora', '1959-03-31', 'L', 'Jumadis', '02143911227', 'JB002', 1, '20190331_1902005.jpeg'),
('1902006', 'Hamzah', 'Bekasi', '1999-09-15', 'L', 'JL Bekasi Indah', '082246484386', 'JB004', 1, 'default.jpeg'),
('1904007', 'Arya', 'Bekasi', '1999-09-11', 'L', 'Bekasi Indah', '02143911227', 'JB001', 1, 'default.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_detail`
--

CREATE TABLE `pembelian_detail` (
  `id_pembelian_d` int(11) NOT NULL,
  `id_pembelian_h` int(11) NOT NULL,
  `kode_barang` varchar(5) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` float NOT NULL,
  `jumlah` float NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_detail`
--

INSERT INTO `pembelian_detail` (`id_pembelian_d`, `id_pembelian_h`, `kode_barang`, `qty`, `harga`, `jumlah`, `flag`) VALUES
(1, 1, 'BR001', 1, 20000, 20000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_header`
--

CREATE TABLE `pembelian_header` (
  `id_pembelian_h` int(11) NOT NULL,
  `no_transaksi` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `kode_supplier` varchar(5) NOT NULL,
  `approved` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_header`
--

INSERT INTO `pembelian_header` (`id_pembelian_h`, `no_transaksi`, `tanggal`, `kode_supplier`, `approved`, `flag`) VALUES
(1, 'TR90520A01', '2019-05-02', 'SP001', 1, 1),
(2, 'TR90520A02', '2019-05-02', 'SP004', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_detail`
--

CREATE TABLE `penjualan_detail` (
  `id_jual_d` int(11) NOT NULL,
  `id_jual_h` int(11) NOT NULL,
  `kode_barang` varchar(5) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` float NOT NULL,
  `jumlah` float NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_detail`
--

INSERT INTO `penjualan_detail` (`id_jual_d`, `id_jual_h`, `kode_barang`, `qty`, `harga`, `jumlah`, `flag`) VALUES
(1, 3, 'BR001', 1, 4500000, 4500000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_header`
--

CREATE TABLE `penjualan_header` (
  `id_jual_h` int(11) NOT NULL,
  `no_transaksi` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `pembeli` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_header`
--

INSERT INTO `penjualan_header` (`id_jual_h`, `no_transaksi`, `tanggal`, `pembeli`, `flag`) VALUES
(1, 'TR90520A01', '2019-05-02', 'Sofyan', 1),
(3, 'TR90520A02', '2019-05-02', 'Ahmad', 1),
(5, 'TR90520A03', '2019-05-02', 'Udin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `kode_supplier` varchar(5) NOT NULL,
  `nama_supplier` varchar(150) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`kode_supplier`, `nama_supplier`, `alamat`, `telp`, `flag`) VALUES
('SP001', 'CV. Budi Luhur', 'JL Kelinci Wangi', '02143911227', 1),
('SP002', 'CV. Informa', 'JL Banteng Gelap', '02143911227', 1),
('SP003', 'Toko Bunga Andi', 'JL Bunga Wangi', '0217312421', 1),
('SP004', 'LP3I', 'Plumpang wangi', '1238129312', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `tipe` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nik`, `email`, `password`, `tipe`, `flag`) VALUES
(1, '1902001', 'adibsee18@gmail.com', 'a43ea2f3c29ef3423c48d633d1a1909d', 2, 1),
(2, 'admin', 'admintoko@gmail.com', 'a43ea2f3c29ef3423c48d633d1a1909d', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`kode_jabatan`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  ADD PRIMARY KEY (`id_pembelian_d`);

--
-- Indexes for table `pembelian_header`
--
ALTER TABLE `pembelian_header`
  ADD PRIMARY KEY (`id_pembelian_h`);

--
-- Indexes for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  ADD PRIMARY KEY (`id_jual_d`);

--
-- Indexes for table `penjualan_header`
--
ALTER TABLE `penjualan_header`
  ADD PRIMARY KEY (`id_jual_h`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`kode_supplier`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  MODIFY `id_pembelian_d` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pembelian_header`
--
ALTER TABLE `pembelian_header`
  MODIFY `id_pembelian_h` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  MODIFY `id_jual_d` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `penjualan_header`
--
ALTER TABLE `penjualan_header`
  MODIFY `id_jual_h` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
